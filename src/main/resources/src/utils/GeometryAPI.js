const PolygonActions = require('../actions/PolygonActions');
const request = require('superagent');

const API_URL = "http://localhost:8080/api/";

const GeometryAPI = {

    fetchPolygons: function() {
        request.get(API_URL + 'feature/polygons')
            .set('Accept', 'application/json')
            .set('Access-Control-Allow-Origin', 'http://localhost')
            .end(function(err, response) {
                if (err) return console.error(err);
               PolygonActions.default.receivePolygons(response.body);
            });
    }

};

module.exports = GeometryAPI;