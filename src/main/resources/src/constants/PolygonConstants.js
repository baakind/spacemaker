const keyMirror = require('keyMirror');

module.exports = keyMirror({
    REQUEST_POLYGONS: null,
    RECEIVE_POLYGONS: null,
    STORE_POLYGONS: null
});