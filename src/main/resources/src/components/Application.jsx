import React from 'react';
import ReactDOM from 'react-dom';
import ReactMapboxGl, { GeoJSONLayer } from "react-mapbox-gl";
import * as MapboxGL from 'mapbox-gl';

import PolygonStore from '../stores/PolygonStore';
import PolygonActions from '../actions/PolygonActions';


const Map = ReactMapboxGl({
    accessToken: "pk.eyJ1IjoiYmFha2luZCIsImEiOiJjamE5cGM1ZXIwZm92MnlqdTYzcGtyd2I1In0.Z7cX9uQXd1iQEo0rFXtkxA",
    zoom: 8
});

const circleLayout: MapboxGL.CircleLayout = { visibility: 'visible' };
const circlePaint: MapboxGL.CirclePaint = {
    'circle-color': 'white'
};

const fillPaint: MapboxGL.FillPaint = {
    'fill-color': '#088',
    'fill-opacity': 0.8
};

const Application = class Application extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            featureCollection: PolygonStore.getPolygons()
        };

        this._onChange = this._onChange.bind(this);
        this._fetchPolygons();
    }

    componentDidMount() {
        PolygonStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        PolygonStore.removeChangeListener(this._onChange);
    }

    _fetchPolygons() {
        PolygonActions.fetchPolygons();
    }

    _onChange() {
        this.setState({
            featureCollection: PolygonStore.getPolygons(),
        });
    }

    _onClickPolygon(e) {
        // TODO Receive coordinates. How to get polygon?
    }

    render() {
        const center = [-0.14007568359375, 51.5027589576403];
        return (
            <Map
                style="mapbox://styles/mapbox/streets-v9"
                center={center}
                containerStyle={{
                    height: "100vh",
                    width: "100vw"
                }}>
                <GeoJSONLayer
                    id="marked-areas"
                    data={this.state.featureCollection}
                    circleLayout={circleLayout}
                    circlePaint={circlePaint}
                    fillPaint={fillPaint}
                    fillOnClick={this._onClickPolygon}
                />
            </Map>
        )
    }
};

ReactDOM.render(<Application />, document.getElementById('application'));