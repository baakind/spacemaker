const AppDispatcher = require('../dispatcher/AppDispatcher');
const PolygonConstants = require('../constants/PolygonConstants');
const ObjectAssign = require('object-assign');
const EventEmitter = require('events').EventEmitter;

const CHANGE_EVENT = 'change';

var _featureCollection = {};

const PolygonStore = ObjectAssign( {}, EventEmitter.prototype, {


    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeChangeListener(CHANGE_EVENT, callback);
    },

    getPolygons: function() {
        return _featureCollection;
    }
});

AppDispatcher.register(function(payload) {

    const action = payload.action;

    switch(action.actionType) {

        case PolygonConstants.RECEIVE_POLYGONS:
            _featureCollection = action.response;
            PolygonStore.emit(CHANGE_EVENT);
            break;
    }
});

module.exports = PolygonStore;