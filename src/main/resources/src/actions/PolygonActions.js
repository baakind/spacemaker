const AppDispatcher = require('../dispatcher/AppDispatcher');
const PolygonConstants = require('../constants/PolygonConstants');
const GeometryAPI = require('../utils/GeometryAPI');

export default {

    fetchPolygons: function() {
       GeometryAPI.fetchPolygons();
    },

    receivePolygons: function(response) {
        AppDispatcher.handleServerAction({
            actionType: PolygonConstants.RECEIVE_POLYGONS,
            response: response
        });
    },

    storePolygons: function(newPolygon, removePolygons) {
        AppDispatcher.handleViewAction({
            actionType: PolygonConstants.STORE_POLYGONS,
            newPolygon,
            removePolygons
        });
    }

};