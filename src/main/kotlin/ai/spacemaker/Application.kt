package ai.spacemaker

import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Application

fun main(args: Array<String>) {
    val log = LoggerFactory.getLogger(Application::class.java)
    log.info("Start Spacemaker.ai")

    SpringApplication.run(Application::class.java, *args)
}