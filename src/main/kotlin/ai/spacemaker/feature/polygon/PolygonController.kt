package ai.spacemaker.feature.polygon

import org.geojson.FeatureCollection
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class PolygonController(val polygonService: PolygonService) {

    @GetMapping("/api/feature/polygons")
    fun getPolygons(): ResponseEntity<FeatureCollection> {
        return ResponseEntity.ok()
            .header("Access-Control-Allow-Origin", "http://localhost")
            .body(polygonService.getPolygons())
    }

}