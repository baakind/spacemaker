package ai.spacemaker.feature.polygon

import org.geojson.FeatureCollection
import org.springframework.stereotype.Service

@Service
class PolygonService(val polygonRepository: PolygonRepository) {

    fun getPolygons(): FeatureCollection = polygonRepository.getPolygons()
}