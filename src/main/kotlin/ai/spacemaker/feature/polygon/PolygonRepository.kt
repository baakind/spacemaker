package ai.spacemaker.feature.polygon

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.geojson.FeatureCollection
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Repository

@Repository
class PolygonRepository(@Value("\${file.geojson}") val geojsonFile: String) {

    private val featureCollection: FeatureCollection

    init {
        val geojsonFileAsStream = PolygonRepository::class.java.getResourceAsStream(geojsonFile)
        featureCollection = ObjectMapper().readValue(geojsonFileAsStream)
    }

    fun getPolygons(): FeatureCollection = featureCollection

}