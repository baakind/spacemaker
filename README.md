# Spacemaker.ai MapUI
MapUI created for the Spacemaker-exercise.

Decisions made:
- Using React as a framework for rendering the view
- Webpack as a build-tool
- Mapbox to draw the map
- Spring boot for the backend-application
- Kotlin as a language for the backend-app
- No database, only a in-memory structure for the features/polygons.

To further improve the scalability of the app, we could do the following:
1. **Separate the frontend and backend into individual applications.** This would allow us to deploy them individually, 
which would let them develop at their own pace. If we put them under the same domain, we wouldn't need CORS-headers, 
even though we could easily add CORS-headers for known hosts without problems.

2. **Introduce a database**. Preferably a cloud-database to store the features/polygons. However, it could be nice to
 use a database with built-in functionality for geojson, e.g., MongoDB, and take advantage of that if we would add 
 more complex features later on. This may be e.g., to find all POI within X meters of a point.
 
3. **Connect the frontend to the backend using websockets**. This will allow us to push realtime-changes to the 
frontend, if e.g., the geojson-layer is changed by one user, we could push it out to all other users of the application.

4. **Improve build-scripts**. We could easily improve the webpack-script. I haven't setup a lot of frontend-projects 
from scratch, which means that I spent some time figuring out how things works today (Last time I worked with a 
build-tool for frontend I was using Gulp).

## Still missing
I have added dependencies (turf) in the frontend to handle union and intersection. However, I still haven't managed 
to implement it, as I got caught up struggling with the map-framework :(. All my time went by finding the right 
map-framework, and wranling with the framework to display the map correctly, and trying to setup interactions with it
. I see now that I maybe should've used another repository for the React-component of MapboxGL, e.g., Uber's: 
https://github.com/uber/react-map-gl.

I didn't quite finnish that job, the only thing that is done is to read the polygons from the backend. And a 
clickhandler is added which triggers when I click on the GeoJSONLayer, but nothing more. I get the coordinates of the
 click from the clickevent, but not the polygon I clicked. I maybe have to use Polygons instead of GeoJSONLayer..
 
However, I planned to store the state of which polygons that was selected in memory, and add two buttons, union and 
intersect (which would be enabled when two polygons is selected).

When clicked, I would perform an intersect or union of those two, and pass the result + the two polygons used to 
generate the result to the backend. The backend would then remove the two polygons that were the foundation of the 
operation from the collection, and add the new one. 

The frontend would then receive a response from the backend about the result of the operation, and if it succeeded, 
it would reload the polygons.


## How to run
1. Build the project: `mvn clean install` (something have to be done with the node_modules. They slow things down 
when they are copied!)
2. Create a dockerimage `docker build -t spacemakerapp .`
3. Run the dockerimage `docker run -d -p 8080:8080`
4. Go to http://localhost:8080